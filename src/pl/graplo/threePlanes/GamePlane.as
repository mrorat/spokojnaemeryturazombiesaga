/**
 * Created by Si00b00l on 3/26/2014.
 */
package pl.graplo.threePlanes {
import pl.graplo.threePlanes.DefaultPlaneBg;

import starling.animation.IAnimatable;
import starling.display.Shape;
import starling.display.Sprite;

public class GamePlane extends Sprite implements IAnimatable{

	private var bgs:Vector.<DefaultPlaneBg>;
	private var scale:Number;

	public function GamePlane(gameWidth:Number,scale:Number) {
		this.scale = scale;
		bgs = new Vector.<DefaultPlaneBg>();
		var bg:DefaultPlaneBg;// = new DefaultPlaneBg();
		var bgLength:Number=0;
		do {
			bg = new DefaultPlaneBg();
			bg.scaleX = bg.scaleY = scale;
			bg.x = bgLength;
			bgLength+=bg.width;
			bgs.push(bg);
			addChild(bg);
		} while (bgLength<gameWidth+bg.width);

	}

	public function advanceTime(time:Number):void {
		var bg:DefaultPlaneBg;
		for each (bg in bgs){
			bg.x-=3*scale;
		}

		bg = bgs[0];
		while (bg.x<-bg.width){
			var shiftedBg:DefaultPlaneBg = bgs.shift();
			shiftedBg.x = bgs[bgs.length-1].x+bgs[bgs.length-1].width;
			bgs.push(shiftedBg);
			bg = bgs[0];
		}
	}
}
}
