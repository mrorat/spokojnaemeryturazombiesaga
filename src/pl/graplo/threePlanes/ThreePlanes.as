/**
 * Created by Si00b00l on 15.01.14.
 */
package pl.graplo.threePlanes {

    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
import flash.display3D.Context3DProfile;
import flash.events.Event;
    import flash.geom.Rectangle;

    import starling.core.Starling;

    [SWF(width="800",height="600",frameRate="60",backgroundColor="#424254")]
    public class ThreePlanes extends Sprite
    {
        public function ThreePlanes()
        {
            if(this.stage)
            {
                this.stage.scaleMode = StageScaleMode.NO_SCALE;
                this.stage.align = StageAlign.TOP_LEFT;
            }
            this.mouseEnabled = this.mouseChildren = false;
            this.loaderInfo.addEventListener(Event.COMPLETE, loaderInfo_completeHandler);
        }

        private var _starling:Starling;

        private function loaderInfo_completeHandler(event:Event):void
        {
            Starling.handleLostContext = true;
            Starling.multitouchEnabled = true;

            this._starling = new Starling(Main, this.stage);//,null,null,"auto",Context3DProfile.BASELINE_EXTENDED);
            this._starling.enableErrorChecking = false;
            this._starling.showStats = true;
            this._starling.simulateMultitouch = true;
            this._starling.start();

            this.stage.addEventListener(Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
            this.stage.addEventListener(Event.DEACTIVATE, stage_deactivateHandler, false, 0, true);
        }

        private function stage_resizeHandler(event:Event):void
        {
            this._starling.stage.stageWidth = this.stage.stageWidth;
            this._starling.stage.stageHeight = this.stage.stageHeight;

            const viewPort:Rectangle = this._starling.viewPort;
            viewPort.width = this.stage.stageWidth;
            viewPort.height = this.stage.stageHeight;
//            trace('starling stage resize',this.stage.stageWidth,this.stage.stageHeight);
            try
            {
                this._starling.viewPort = viewPort;
            }
            catch(error:Error) {}
        }

        private function stage_deactivateHandler(event:Event):void
        {
            this._starling.stop();
            this.stage.addEventListener(Event.ACTIVATE, stage_activateHandler, false, 0, true);
        }

        private function stage_activateHandler(event:Event):void
        {
            this.stage.removeEventListener(Event.ACTIVATE, stage_activateHandler);
            this._starling.start();
        }

    }
}