/**
 * Created by Si00b00l on 3/26/2014.
 */
package pl.graplo.threePlanes {
import starling.display.Shape;
import starling.display.graphicsEx.ShapeEx;

public class DefaultPlaneBg extends Shape{
	public function DefaultPlaneBg() {
		graphics.beginFill(0xcccccc);
		graphics.lineStyle(2,0xccddcc);
		graphics.drawRect(0,0,100,100);
		graphics.endFill();
	}
}
}
