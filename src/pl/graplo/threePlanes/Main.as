/**
 * Created by Si00b00l on 3/26/2014.
 */
package pl.graplo.threePlanes {

import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;
import starling.textures.Texture;

public class Main extends Sprite {
	private var btnStart:Button;
	private var game:Game;
	public function Main() {

		addEventListener(Event.ADDED_TO_STAGE, addedToStage_handler);
	}

	private function addedToStage_handler(event:Event):void {
		game = new Game();
		addChild(game);
		game.visible=false;
		var texture:Texture = Texture.fromColor(100,50,0xffcc00ff);
		btnStart = new Button(texture,"Start");
		addChild(btnStart);
		btnStart.alignPivot();
		btnStart.x = stage.stageWidth/2;
		btnStart.y = stage.stageHeight/2;

		btnStart.addEventListener(Event.TRIGGERED, btnStartTriggered_handler);


	}

	private function btnStartTriggered_handler(event:Event):void {
		btnStart.visible = false;
		game.visible=true;
	}
}
}
