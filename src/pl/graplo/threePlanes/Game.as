/**
 * Created by Si00b00l on 3/26/2014.
 */
package pl.graplo.threePlanes {
import starling.animation.Juggler;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class Game extends Sprite{
	private var gameJuggler:Juggler;
	private var plane1:GamePlane;
	private var plane2:GamePlane;
	private var plane3:GamePlane;
	private var hero:Hero;
	private var planes:Vector.<GamePlane>;

//	private var _currPlane:GamePlane;
	private var _currPlane:int;

	public function Game() {
		gameJuggler = new Juggler();
		addEventListener(Event.ADDED_TO_STAGE, addedToStage_handler);
	}

	private function addedToStage_handler(event:Event):void {

		plane1 = new GamePlane(stage.stageWidth,1);
		plane2 = new GamePlane(stage.stageWidth,.8);
		plane3 = new GamePlane(stage.stageWidth,.6);
		planes = Vector.<GamePlane>( [plane1, plane2, plane3] );
		addChild(plane3);
		addChild(plane2);
		addChild(plane1);
		plane1.y = stage.stageHeight-plane1.height-10;
		plane2.y = plane1.y-plane2.height-10;
		plane3.y = plane2.y-plane3.height-10;

		hero = new Hero();
		addChild(hero);
		hero.x = 100;
//		currPlane = plane2;
		currPlane = 1;

		gameJuggler.add(plane1);
		gameJuggler.add(plane2);
		gameJuggler.add(plane3);
		start();
	}

	private function start():void{
		addEventListener(EnterFrameEvent.ENTER_FRAME, enterFrame_handler);
		stage.addEventListener(TouchEvent.TOUCH, touch_handler);
	}

	private function touch_handler(event:TouchEvent):void {
		var touch:Touch = event.touches[0];
		if (touch.phase == TouchPhase.BEGAN){
			if (touch.globalX<stage.stageWidth/2){
				currPlane++;
			} else {
				currPlane--;
			}
		}
//		trace('touch',event);
	}

	private function enterFrame_handler(event:EnterFrameEvent):void {
		gameJuggler.advanceTime(event.passedTime);

	}

//	public function set currPlane(value:GamePlane):void {
//		_currPlane = value;
//		hero.y = _currPlane.y;
//	}
	public function set currPlane(value:int):void {
		if (value>planes.length-1 || value<0){
			return;
		}
		_currPlane = value;
		hero.y = planes[_currPlane].y;
	}

	public function get currPlane():int {
		return _currPlane;
	}
}
}
